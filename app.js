const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');

const indexRouter = require('./routes/index');
const heartbeatRouter = require('./routes/heartbeat');
const memoryTestRouter = require('./routes/memoryTest');
const gatewayRouter = require('./routes/gateway');
const oddRouter  = require('./routes/odd');
const cpuRouter = require('./routes/cpuTest');

const userRouter = require('./UserRoutes/user');

const app = express();


mongoose.connect('mongodb://localhost/testDB', { useNewUrlParser: true });
mongoose.Promise = global.Promise;


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/heartbeat', heartbeatRouter);
app.use('/memoryTest', memoryTestRouter);
app.use('/gateway', gatewayRouter);
app.use('/odd', oddRouter);
app.use('/cpuTest', cpuRouter);

app.use('/user', userRouter);


app.use(function(err, req, res, next){

});


app.listen(process.env.port || 4000, function(){
    console.log('now listening for requests...');

});

module.exports = app;
