const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create new User schema
const UserLoginSchema = new Schema({
    user_id: {
        type: String,
        required: [true, "Id is Required"]
    },

    full_name: {
        type: String
    },

    email: {
        type: String
    },

    password: {
        type: String
    },

    user_type: {
        type: String
    },

});

const UserLogin = mongoose.model('userLogin', UserLoginSchema);

module.exports = UserLogin;