const express = require('express');
const odd_router = express.Router({});
const Odd = require('../models/odd_model');


var mongodb = require('mongodb').MongoClient;

const http = require('http-server');


/* GET users listing. */
odd_router.get('/', function (req, res) {
    Odd.find({}).then(function(odd){
        res.send(odd);
    });

});

odd_router.get('/:gw_uuid', function (req, res) {
    Odd.find({gw_uuid : req.params.gw_uuid}).then(function(odd){
        res.send(odd);
    });

});

odd_router.post('/', function (req, res) {
  //  var odd = new Odd(req.body);
  //  odd.save();
    Odd.create(req.body);

    res.send(req.body);
});


odd_router.put('/', function (req, res) {
    var query = {gw_uuid : req.body.gw_uuid};
    Odd.updateMany(query, {gw_uuid : req.body.gw_uuid, filename : req.body.filename}).then(function(odd) {
        res.send(odd);
    });
});

odd_router.delete('/', function (req, res) {
    Odd.delete(req.body).then(function() {
        res.send(req.body);
    });

    //res.status(202).send();
    //process.exit();
});

odd_router.patch('/', function (req, res) {
    res.status(202).json(req.body);
});

odd_router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = odd_router;