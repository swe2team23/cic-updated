const express = require('express');
const router = express.Router({});
const Heartbeat = require('../models/heartbeat_model');
const Odd = require('../models/odd_model');


var mongodb = require('mongodb').MongoClient;

const http = require('http-server');


/* GET users listing. */
router.get('/', function (req, res) {
    Heartbeat.find({}).then(function(heartbeat){
        res.send(heartbeat);
       // console.log(heartbeat);
    });
});

router.post('/', function (req, res) {
   // var heartbeat = new Heartbeat(req.body);
  //  heartbeat.save();
    Heartbeat.create(req.body);

    var query = {gw_uuid : req.body.gw_uuid};
    Odd.find(query).then(function(odd){
        Odd.update(query, { $unset: { filename: '' }});
        res.send(odd);
        db.odds.update(query, {$unset: { filename: '' }}, {multi : true});
    });
});




router.put('/', function (req, res) {
    res.status(202).send();
});

router.delete('/:id', function (req, res) {
    console.log(req.params.id);
    req.send({type: 'DELETE'});
   // res.end();
    //process.exit();
});

router.patch('/', function (req, res) {
    res.status(202).json(req.body);
});

router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = router;
