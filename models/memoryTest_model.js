const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create new On-Demand CPU schema
const MemoryTestSchema = new Schema({
    gw_uuid: {
        type: String,
        required: [true, "Id is Required"]
    },

    result: {
        type: String
    },

    timestamp : {
        type: String
    }
});

const MemoryTest = mongoose.model('memoryTest', MemoryTestSchema);

module.exports = MemoryTest;