const express = require('express');
const userRouter = express.Router({});
const User = require('../UserModel/user_model');



var mongodb = require('mongodb').MongoClient;

const http = require('http-server');


/* GET users listing. */
userRouter.get('/', function (req, res) {
    User.find({}).then(function(user){
        res.send(user);
    });

});

userRouter.get('/:user_id', function (req, res) {
    var query = {user_id : req.params.user_id};
    User.find(query).then(function(user){
        res.send(user);
    });

});

userRouter.post('/', function (req, res) {
    User.create(req.body).then(function(user) {
        res.send(user);
    });


});

userRouter.put('/:user_id', function (req, res) {
    var query = {user_id : req.params.user_id};
    User.updateOne(query, req.body).then(function(user){
        res.send(user);
    });
});

userRouter.delete('/:user_id', function (req, res) {
    console.log(req.params.id);
    req.send({type: 'DELETE'});
    // res.end();
    //process.exit();
});

userRouter.patch('/', function (req, res) {
    res.status(202).json(req.body);
});

userRouter.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = userRouter;
